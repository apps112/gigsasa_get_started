.. image:: logo.png
	:align: center


Getting Started with Gigsasa!
=============================


Welcome to Gigsasa! 
We are glad to have you on board. Gigsasa is a platform for time tracking, activity tracking, and team productivity.
Gigsasa gives you the tools needed for your team to optimize its productivity.It can be hard to learn a new platform, but don’t worry! 
This guide will provide all the information to succeed in optimizing your business. 

Contents:
=========

.. toctree::
   :maxdepth: 1
   
   Getting Started
   Create and set up your team
   Invite team members
   Create Job Sites
   Creating Shift
   Timesheet Management
   Monitor and track productivity
   Manage your account
 



Links:
======

* **Web Portal**
	* `Website <https://gigsasa.com/>`_
