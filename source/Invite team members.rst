Invite team members
===================
You can invite team members to the workspace. When you invite team members they will receive a mail to join the workspace. On joining the workspace, Gigsasa tools help you to track team activities with few clicks.
To add team members, click on the team icon on the sidebar

                .. image:: dashboard-members.png
                  :align: center

Next, invite team members by email or importing a CSV file
                        .. image:: team-button.png
                          :align: center

Team members details is filled and added to the team  

.. image:: create-team.png
  :align: center

