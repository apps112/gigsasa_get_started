Creating Shift
==============
Admins can create, edit shifts for teams and department
To create shifts, click the Icon with schedules and shifts on the sidebar. Below is a screenshot  

.. image:: dashboard-shift.png
  :align: center

Shifts can be created by clicking the create shift button, already created shifts can
filtered base on days, weeks, and team members.

.. image:: view-shift.PNG
  :align: center


