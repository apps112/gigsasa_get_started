Monitor and Track Productivity
==============================
Welcome to Gigsasa! 
We are glad to have you on board. Gigsasa is a platform for time tracking, activity tracking, and team productivity.
Gigsasa gives you the tools needed for your team to optimize its productivity.
It can be hard to learn a new platform, but don’t worry! 
This guide will provide all the information to succeed in optimizing your business. 


How to track team progress and productivity 
===========================================
The following screenshot gives admins added advantage to track and manage the productivity of their teams. To track the productivity of members, admins make use of the dashboard metrics

.. image:: dash-track.PNG
  :align: center

The metrics gives the admin all the necessary information needed to make conclusions on a particular tem member.  Admins can also filter department, teams and metrics of persons present and working

