Assign Roles
============
Admins can assign roles to team members to help coordinate the other department. This helps relieve admin to pay attention to more important needs of the company.
To assign roles to team members, click on the teams icon on the sidebar to open up teams
page, then click on the 3 dots under actions 

.. image:: assign-role.png
  :align: center


