Manage your account
===================
Welcome to Gigsasa! 
We are glad to have you on board. Gigsasa is a platform for time tracking, activity tracking, and team productivity.
Gigsasa gives you the tools needed for your team to optimize its productivity.It can be hard to learn a new platform, but don’t worry! 
This guide will provide all the information to succeed in optimizing your business. 

How to manage your account
**************************

Below are screenshots on how to manage your account. All users can edit account and update it. To edit your accout, click on the Icon on the dashboard with settings.

.. image:: dashboard-settings.png
  :align: center

Below are screenshots on how to manage your account. All users can edit account and update it. To edit your accout, click on the Icon on the dashboard with settings.

.. image:: company-settings.png
  :align: center


